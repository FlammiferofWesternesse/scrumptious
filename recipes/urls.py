from django.urls import path
from .views import (create_recipes, edit_recipe,
                   recipe_list, show_recipe)

urlpatterns = [
    path("create/", create_recipes, name='create_recipes'),
    path("<int:id>/edit/", edit_recipe, name='edit_recipe'),
    path("", recipe_list, name='recipe_list'),
    path("<int:id>/", show_recipe, name='show_recipe'),
]
