from django.db import models

# Create your models here.

class Recipe(models.Model):
    title = models.CharField(max_length=50)
    picture = models.URLField()
    picture_alt_text = models.CharField(max_length=200, null=True)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
